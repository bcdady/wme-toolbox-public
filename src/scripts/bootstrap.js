// -------------------------------------------------------------------------------
// Comment because Chrome v59 bug 
// see https://www.waze.com/forum/viewtopic.php?f=819&t=145570&start=1240#p1665008
// -------------------------------------------------------------------------------

var getnav = function() {
    var ua = navigator.userAgent,
        tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
            //if(tem!= null) return 'Opera '+tem[1];
        if (tem != null) return 'Opera';
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    //return M.join(' ');
    return M[0];
};

var GoodBrowser = true;

var getURL = function(url) {
    if (GoodBrowser) {
        switch (getnav()) {
            case "Firefox":
                if (typeof url == "string") {
                    return (browser.extension.getURL(url));
                }
                break;
            case "Safari":
                if (typeof url == "string") {
                    return (safari.extension.baseURI + url);
                }
                break;
            case "Chrome":
                if (typeof url == "string") {
                    return (chrome.extension.getURL(url));
                }
                break;
            case "Opera":
                if (typeof url == "string") {
                    return (chrome.extension.getURL(url));
                }
                break;
            default:
                alert("WME Toolbox: your browser " + getnav() + " is not compatible.");
                GoodBrowser = false;
                return (false);
                break;
        }
    }
};

getURL();

// Check if another instance of Tb is already running
if (document.getElementsByName("WMETB_Red0.png").length > 0) {
    alert("WME Toolbox:\n\nA second instance of this extension was launched, which cannot work.\nGiving up.\n\nPlease ensure that only one instance of WME Toolbox is enabled.");
} else {

    /************************/
    /*** COMMON LIBRARIES ***/
    /************************/

    var lib = document.createElement('script');
    lib.src = getURL("scripts/wmelib.js");
    lib.async = false;
    lib.onload = function() {};
    document.head.appendChild(lib);

    /***********************************/
    /*** READY TO INJECT TOOLBOX CSS ***/
    /***********************************/

    document.addEventListener('DOMInjectCss', injectCss, false);

    function injectCss() {
        if (document.head) {
            document.removeEventListener('DOMInjectCss', injectCss, false);

            var s = document.createElement('link');
            s.type = 'text/css';
            s.rel = 'stylesheet';
            s.href = getURL("css/toolbox2.css");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);
        }
    }

    /*******************************************/
    /*** READY TO INJECT JQUERY-UI IF NEEDED ***/
    /*******************************************/

    document.addEventListener('DOMInjectJquery', injectJquery, false);

    function injectJquery() {
        if (document.head) {
            document.removeEventListener('DOMInjectJquery', injectJquery, false);

            var s = document.createElement('script');
            s.src = getURL("scripts/jquery-ui-1.11.4.custom.min.js");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

            var s = document.createElement('link');
            s.type = 'text/css';
            s.rel = 'stylesheet';
            s.href = getURL("css/jquery-ui-1.11.4.custom.min.css");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);
        }
    }

    /*************************************/
    /*** READY TO INJECT JNF IF NEEDED ***/
    /*************************************/

    /* if (location.href.indexOf("beta.waze.com") !== -1) { // Non Beta
        document.addEventListener('DOMInjectJnf', injectJnf, false);
        function injectJnf(){
            if(document.head){
                document.removeEventListener('DOMInjectJnf', injectJnf, false);

                var s = document.createElement('script');
                    s.src = getURL("scripts/toolbox_jnf2_include.min.js");
                    s.async = false;
                    s.onload = function() {
                };
                document.head.appendChild(s);
            }
        }
    } */

    /********************************/
    /*** PICTURES USED IN TOOLBOX ***/
    /********************************/

    WMETB_SetImage = function(ImageName) {
        var img = document.createElement('img');
        img.src = getURL('images/' + ImageName);
        img.name = ImageName;
        img.async = false;
        img.onload = function() {}
        document.head.appendChild(img);
    }

    console.log('WMETB: loading pictures...');
    WMETB_SetImage("layer_help.png");
    WMETB_SetImage("TimeRestrictedNode.png");
    WMETB_SetImage("TimeRestrictedNodeOld.png");
    WMETB_SetImage("DeadEndNode.png");
    WMETB_SetImage("RevConnNode.png");
    WMETB_SetImage("UTurnNode.png");
	WMETB_SetImage("MP.png");
    WMETB_SetImage("UnconfirmedNode.png");
    WMETB_SetImage("TTSOverride.png");
    WMETB_SetImage("cp.png");
    WMETB_SetImage("lockl2.png");
    WMETB_SetImage("lockl2A.png");
    WMETB_SetImage("lockl3.png");
    WMETB_SetImage("lockl3A.png");
    WMETB_SetImage("lockl4.png");
    WMETB_SetImage("lockl4A.png");
    WMETB_SetImage("lockl5.png");
    WMETB_SetImage("lockl5A.png");
    WMETB_SetImage("lockl6.png");
    WMETB_SetImage("lockl6A.png");
    WMETB_SetImage("lock_green.png");
    WMETB_SetImage("lock_red.png");
    WMETB_SetImage("manuallock.png");
    WMETB_SetImage("gyro.gif");
    WMETB_SetImage("sidebar_arrow_left.png");
    WMETB_SetImage("sidebar_arrow_right.png");
    WMETB_SetImage("keyboard.png");
    WMETB_SetImage("RDB_Blue.png");
    WMETB_SetImage("RDB_Grey.png");
    WMETB_SetImage("RDB_Redo.png");
    WMETB_SetImage("RDB_StandardRoad.png");
    WMETB_SetImage("RDB_Select.png");
    WMETB_SetImage("RDB_Landmark.png");
    WMETB_SetImage("Clear_Road_Geometry.png");
    WMETB_SetImage("Clear_Road_Geometry_Grey.png");
    WMETB_SetImage("Split_Road.png");
    WMETB_SetImage("Split_Road_Grey.png");
    WMETB_SetImage("WMETB_CreateJunctions.png");
    WMETB_SetImage("Select_In_Landmark.png");
    WMETB_SetImage("Select_In_Landmark_Grey.png");
    WMETB_SetImage("WMETB_magicClearNodes.png");
    WMETB_SetImage("WMETB_magicFixUturns.png");
    WMETB_SetImage("WMETB_magicFixOthers.png");
    WMETB_SetImage("WMETB_magicFixLoops.png");
    WMETB_SetImage("WMETB_magicFixToll.png");
    WMETB_SetImage("WMETB_SuppressUnneededGeometry.png");
    WMETB_SetImage("WMETB_SuppressUnneededGeometry_Grey.png");
    WMETB_SetImage("WMETB_AllowAllConnectionsInLandmark.png");
    WMETB_SetImage("WMETB_AllowAllConnectionsOnScreen.png");
    WMETB_SetImage("WMETB_CopySegmentAttributes.png");
    WMETB_SetImage("WMETB_CopySegmentAttributes_Grey.png");
    WMETB_SetImage("WMETB_CopyVenueAttributes.png");
    WMETB_SetImage("wipe.png");
    WMETB_SetImage("WMETB_DeletePastRestrictions.png");
    WMETB_SetImage("select.png");
    WMETB_SetImage("select_venue.png");
    WMETB_SetImage("pe_on.png");
    WMETB_SetImage("pe_off.png");
    WMETB_SetImage("power_green.png");
    WMETB_SetImage("power_red.png");
    WMETB_SetImage("10arrowup.gif");
    WMETB_SetImage("10arrowdown.gif");
    WMETB_SetImage("LM.png");
    WMETB_SetImage("beta.png");
    WMETB_SetImage("B.png");
    WMETB_SetImage("G.png");
    WMETB_SetImage("H.png");
    WMETB_SetImage("Gmaker.png");
    WMETB_SetImage("I.png");
    WMETB_SetImage("M.png");
    WMETB_SetImage("Mappy.png");
    WMETB_SetImage("O.png");
    WMETB_SetImage("V.png");
    WMETB_SetImage("Y.png");
    WMETB_SetImage("oamtc.png");
    WMETB_SetImage("ch.png");
    WMETB_SetImage("mapy.png");
    WMETB_SetImage("Maplink.png");
    WMETB_SetImage("wikimapia.png");
    WMETB_SetImage("redLatLonImg.png");
    WMETB_SetImage("pmlink.png");
    WMETB_SetImage("redpermalinkmulti.png");
    WMETB_SetImage("smallredpermalink.png");
    WMETB_SetImage("unlocklink.png");
    WMETB_SetImage("bubblelink.png");
    WMETB_SetImage("squarelink.png");
    WMETB_SetImage("redlink.png");
    WMETB_SetImage("FP_redLink.png");
    WMETB_SetImage("FP_redboxLink.png");
    WMETB_SetImage("FP_redbubbleALL.png");
    WMETB_SetImage("FP_redbubbleLink.png");
    WMETB_SetImage("FP_redbubblePM.png");
    WMETB_SetImage("FP_closureLink.png");
    WMETB_SetImage("FP_unlockLink.png");
    WMETB_SetImage("FP_redlonlat.png");
    WMETB_SetImage("FP_MURLink.png");
    WMETB_SetImage("search-icon-green.png");
    WMETB_SetImage("search-icon-red.png");
    WMETB_SetImage("search-icon-grey.png");
    WMETB_SetImage("WMETB_Red0.png");
    WMETB_SetImage("WMETB_Red1.png");
    WMETB_SetImage("WMETB_Red2.png");
    WMETB_SetImage("WMETB_Red3.png");
    WMETB_SetImage("WMETB_Red4.png");
    WMETB_SetImage("WMETB_Red5.png");
    WMETB_SetImage("WMETB_Red6.png");
    WMETB_SetImage("WMETB_Red7.png");
    WMETB_SetImage("WMETB_Red8.png");
    WMETB_SetImage("WMETB_Red9.png");
    WMETB_SetImage("WMETB_Green0.png");
    WMETB_SetImage("WMETB_Green1.png");
    WMETB_SetImage("WMETB_Green2.png");
    WMETB_SetImage("WMETB_Green3.png");
    WMETB_SetImage("WMETB_Green4.png");
    WMETB_SetImage("WMETB_Green5.png");
    WMETB_SetImage("WMETB_Green6.png");
    WMETB_SetImage("WMETB_Green7.png");
    WMETB_SetImage("WMETB_Green8.png");
    WMETB_SetImage("WMETB_Green9.png");
    WMETB_SetImage("WMETB_Ruler.png");
    WMETB_SetImage("TransparentArrow.png");
    WMETB_SetImage("2Arrows.png");
    WMETB_SetImage("2RedArrows.png");
    WMETB_SetImage("MultipleSelection.png");
    WMETB_SetImage("reporting.png");

    //Images for DataTables
    WMETB_SetImage("background.png");
    // WMETB_SetImage("copy_csv_xls.swf");
    WMETB_SetImage("sort_asc.png");
    WMETB_SetImage("sort_asc_disabled.png");
    WMETB_SetImage("sort_both.png");
    WMETB_SetImage("sort_desc.png");
    WMETB_SetImage("sort_desc_disabled.png");



    /*************************************/
    /*** READY TO INJECT FP ***/
    /*************************************/

    document.addEventListener('DOMInjectFP', injectFP, false);

    function injectFP() {
        if (document.head) {
            document.removeEventListener('DOMInjectFP', injectFP, false);

            var s = document.createElement('script');
            s.src = getURL("scripts/FancyPermalink.min.js");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);
        }
    }

    // console.log('WMETB: '+ "injecting FP");
    // var main = document.createElement('script');
    // main.src = getURL("scripts/FancyPermalink.min.js");
    // main.async = false;
    // main.onload = function() {
    // run_FP();
    // };
    // document.head.appendChild(main);
    // console.log('WMETB: '+ "running FP");

    /********************************************/
    /*** READY TO INJECT DATATABLES IF NEEDED ***/
    /********************************************/

    document.addEventListener('DOMInjectDataTables', injectDataTables, false);

    function injectDataTables() {
        if (document.head) {
            document.removeEventListener('DOMInjectDataTables', injectDataTables, false);

            var s = document.createElement('script');
            s.src = getURL("scripts/jquery.dataTables-1.10.4.min.js");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

            var s = document.createElement('link');
            s.type = 'text/css';
            s.rel = 'stylesheet';
            s.href = getURL("css/jquery.dataTables-1.10.4.min.css");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

            var s = document.createElement('script');
            s.src = getURL("scripts/dataTables.tableTools-2.2.3.min.js");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

            var s = document.createElement('link');
            s.type = 'text/css';
            s.rel = 'stylesheet';
            s.href = getURL("css/dataTables.tableTools-2.2.3.min.css");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

            var s = document.createElement('script');
            s.src = getURL("scripts/dataTables.fixedHeader-2.1.2.min.js");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

            var s = document.createElement('link');
            s.type = 'text/css';
            s.rel = 'stylesheet';
            s.href = getURL("css/dataTables.fixedHeader-2.1.2.min.css");
            s.async = false;
            s.onload = function() {};
            document.head.appendChild(s);

        }
    }

    /*************************/
    /*** LAUNCHING TOOLBOX ***/
    /*************************/
    if (location.href.indexOf("beta.waze.com") === -1) { // Non Beta
        var secure = document.createElement('script');
        secure.src = getURL("scripts/secure.prod.min.js");
        secure.async = false;
        secure.onload = function() {
            var main = document.createElement('script');
            main.src = getURL("scripts/WME_Toolbox.prod.min.js");
            main.async = false;
            main.onload = function() {};
            document.head.appendChild(main);
        };
        document.head.appendChild(secure);
    } else { // Beta
        var secure = document.createElement('script');
        secure.src = getURL("scripts/secure.beta.min.js");
        secure.async = false;
        secure.onload = function() {
            var main = document.createElement('script');
            main.src = getURL("scripts/WME_Toolbox.beta.min.js");
            main.async = false;
            main.onload = function() {};
            document.head.appendChild(main);
        };
        document.head.appendChild(secure);
    }

    /****************************************/
    /*** FANCY PERMALINK COUNTRY SETTINGS ***/
    /****************************************/

    var WMETBFP_countrySettings = {};


    function createCORSRequest(method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {

            // Check if the XMLHttpRequest object has a "withCredentials" property.
            // "withCredentials" only exists on XMLHTTPRequest2 objects.
            xhr.open(method, url, true);

        } else if (typeof XDomainRequest != "undefined") {

            // Otherwise, check if XDomainRequest.
            // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
            xhr = new XDomainRequest();
            xhr.open(method, url);

        } else {

            // Otherwise, CORS is not supported by the browser.
            xhr = null;

        }
        return xhr;
    }

    // http://stackoverflow.com/questions/8493195/how-can-i-parse-a-csv-string-with-javascript
    function CSVtoArray(text) {
        var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
        var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
        // Return NULL if input string is not well formed CSV string.
        if (!re_valid.test(text)) return null;
        var a = []; // Initialize array to receive values.
        text.replace(re_value, // "Walk" the string using replace with callback.
            function(m0, m1, m2, m3) {
                // Remove backslash from \' in single quoted values.
                if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
                // Remove backslash from \" in double quoted values.
                else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                else if (m3 !== undefined) a.push(m3);
                return ''; // Return empty string.
            });
        // Handle special case of empty last value.
        if (/,\s*$/.test(text)) a.push('');
        return a;
    };

    // function to parse a country sheet
    function parseFPCsv(d) {
        // look for the sheet in countrysettings (thanks to the download link)
        for (var i in WMETBFP_countrySettings) {
            if (!WMETBFP_countrySettings.hasOwnProperty(i))
                continue;
            if (d.url == WMETBFP_countrySettings[i].link) {
                //WMETBFP_countrySettings[i].csv=d.data;
                var lines = d.data.split('\n');
                //WMETBFP_countrySettings[i].data=[];
                var labelLine = CSVtoArray(lines[0]);
                for (var l = 1; l < lines.length; l++) {
                    // hack to pass the CSVtoArray regexp!
                    lines[l] = lines[l].replace(/\\/g, "{backslash}");
                    lines[l] = lines[l].replace(/'/g, "{apostrophe}");
                    var dataLine = CSVtoArray(lines[l]);
                    if (!dataLine) 
                    {
                        console.error("WMETB: FP: can't parse CSV line " + l + " (country " + i + "). Jump to next line...", lines[l]);
                        continue;   
                    }
                    var linkType = null;
                    var inState = null;
                    for (var c = 0; c < dataLine.length; c++) {
                        if (c == 0) {
                            linkType = dataLine[c];
                            if (linkType == "") // ignore empty cell
                                break;
                            if (!WMETBFP_countrySettings[i].hasOwnProperty(linkType))
                                WMETBFP_countrySettings[i][linkType] = {}; // create empty object
                        }
                        if (c == 1) {
                            if (dataLine[c] == "") // all states conf
                            {
                                WMETBFP_countrySettings[i][linkType].all = {};
                                inState = WMETBFP_countrySettings[i][linkType].all;
                            } else // per state conf
                            {
                                WMETBFP_countrySettings[i][linkType][dataLine[c]] = {};
                                inState = WMETBFP_countrySettings[i][linkType][dataLine[c]];
                            }
                        }
                        if (c > 1) // just copy cell data in object member named as the column label
                        {
                            inState[labelLine[c]] = dataLine[c];
                        }
                    }
                    //WMETBFP_countrySettings[i].data.push(CSVtoArray(lines[l]));
                }
                //delete WMETBFP_countrySettings[i].csv;
                delete WMETBFP_countrySettings[i].link;
                //delete WMETBFP_countrySettings[i].data;
            }
        }
    }

    // download the google spreadsheet in xml format
    var xhr = createCORSRequest('GET', "https://spreadsheets.google.com/feeds/worksheets/1ZypSvx3S-3ebjTpfjtXl3Zhd-SQjp4kA-IyIFUasxvM/private/values");
    if (!xhr) {
        throw new Error('CORS not supported');
    } else {
        xhr.onload = function() {
            var responseText = xhr.responseText;
            //console.log("WMETB XHR: " + responseText);
            // process the response.

            // setup the XMl parser
            var parseXml;

            if (typeof window.DOMParser != "undefined") {
                parseXml = function(xmlStr) {
                    return (new window.DOMParser()).parseFromString(xmlStr, "text/xml");
                };
            } else if (typeof window.ActiveXObject != "undefined" &&
                new window.ActiveXObject("Microsoft.XMLDOM")) {
                parseXml = function(xmlStr) {
                    var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                    return xmlDoc;
                };
            } else {
                parseXml = null;
                throw new Error("No XML parser found");
            }

            if (parseXml === null) {
                console.log("WMETB: FP: error: no XML Parser found.");
                return;
            }
            var currentCountry = null;
            var xml = null;
            try {
                xml = parseXml(responseText);
            } catch (err) {
                console.debug("WMETB: FP: error while parsing sheets from google doc.", err);
                xml = null;
            }
            if (xml !== null) {
                // look for entry.title and entry.link: it describes sheets
                for (var i = 0; i < xml.childNodes[0].childNodes.length; i++) {
                    var node = xml.childNodes[0].childNodes[i];
                    if (node.nodeName == "entry") {
                        for (var j = 0; j < node.childNodes.length; j++) {
                            var sheet = node.childNodes[j];
                            if (sheet.nodeName == "title") {
                                //console.debug("Title:", sheet.innerHTML);
                                // get the country ID in the sheet name
                                var title = sheet.innerHTML.split(' ');
                                if (title.length > 1) {
                                    var countryID = parseInt(title[0]);
                                    if (!isNaN(countryID)) {
                                        currentCountry = countryID;
                                        WMETBFP_countrySettings['' + currentCountry] = {};
                                    }
                                }
                            }
                            if (sheet.nodeName == "link" && currentCountry !== null) {
                                var link = sheet.getAttribute("href");
                                // add the download to csv link to the coutry settings object
                                if (link.indexOf("format=csv") != -1) {
                                    //console.debug("link:", link);
                                    WMETBFP_countrySettings['' + currentCountry].link = link;
                                }
                            }
                        }
                    }
                }

                // now, download all sheets one by one
                var fpcsArray = Object.keys(WMETBFP_countrySettings);
                //console.debug("WMETBFP WMETBFP_countrySettings" , WMETBFP_countrySettings);
                //console.debug("WMETBFP fpcsArray" , fpcsArray);
                // use an array of XHR objects because
                // multiple requests with the same XHR object leads to variable scope troubles
                var tmpXhr = [];
                for (var i = 0; i < fpcsArray.length; i++) {
                    if (!WMETBFP_countrySettings.hasOwnProperty(fpcsArray[i]))
                        continue;
                    if (WMETBFP_countrySettings[fpcsArray[i]].hasOwnProperty('link')) {
                        var index = i;
                        tmpXhr.push(createCORSRequest('GET', WMETBFP_countrySettings[fpcsArray[i]].link));
                        tmpXhr[tmpXhr.length - 1].onload = (function(url, lastXhr) {
                            //console.debug("WMETBFP onload csv" , arguments);
                            return function() {
                                //console.error('xhr', tmpXhr[lastXhr]);
                                if (tmpXhr[lastXhr].status==429) // rate limit of gg spreadsheet service
                                {
                                    // first, save the handler:
                                    var xhrOnload = tmpXhr[lastXhr].onload;
                                    // set again the xhr object:
                                    tmpXhr[lastXhr]=createCORSRequest('GET', url);
                                    // restore the handler
                                    tmpXhr[lastXhr].onload=xhrOnload;
                                    // try again in 2 secs:
                                    window.setTimeout(function () {
                                        tmpXhr[lastXhr].send();
                                    }, 2000);
                                    return;
                                }
                                var responseText = tmpXhr[lastXhr].responseText;
                                // create a small object with url and data, and send it to the FP csv parser
                                var d = {
                                    url: url,
                                    data: responseText
                                };
                                //console.debug("WMETBFP onload csv d" , d);
                                try {
                                    parseFPCsv(d);
                                } catch (err) {
                                    console.error("WMETB: FP: Error while parsing sheet of country " + fpcsArray[index], err);
                                    // delete link
                                    delete WMETBFP_countrySettings[fpcsArray[index]].link;
                                    
                                }
                                // check if all links have been deleted, i.e. all sheets downloaded and parsed
                                var lastDownload = true;
                                for (var c in WMETBFP_countrySettings) {
                                    if (WMETBFP_countrySettings.hasOwnProperty(c)) {
                                        // link member is deleted only when the link has been downloaded AND parsed.
                                        // if a link still exists, that means it remains something to (download and) parse
                                        if (WMETBFP_countrySettings[c].hasOwnProperty('link')) {
                                            lastDownload = false;
                                            break;
                                        }
                                    }
                                }
                                if (lastDownload) {
                                    // inject script element containing the data structure as a global var
                                    // so FP will read it if it exists
                                    var fpcs = JSON.stringify(WMETBFP_countrySettings);
                                    var tmp = document.createElement("script");
                                    var code = 'window.WMETBFP_countrySettings=' + fpcs + ';';
                                    //console.debug("WMETBFP code" , code);
                                    tmp.textContent = code;
                                    tmp.setAttribute("type", "application/javascript");
                                    document.body.appendChild(tmp);
                                }
                                else
                                {
                                    if (lastXhr != tmpXhr.length-1) // start next download in 1 sec
                                        window.setTimeout(function () {
                                            tmpXhr[lastXhr+1].send();
                                        }, 1000);
                                }
                            };
                        })(WMETBFP_countrySettings[fpcsArray[i]].link, tmpXhr.length - 1);

                    }

                }
                // start the 1st download:
                tmpXhr[0].send();

            }
        };

        xhr.onerror = function() {
            console.log('WMETB: FP: XHR: There was an error!');
        };
        //console.log('WMETB XHR SEND!');
        xhr.send();
    }

    console.log('WMETB: starting...');
}